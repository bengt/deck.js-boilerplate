# deck.js-boilerplate

Wiring deck.js, deck.js-markdown and deck.js-transition-cube together.

## Downloading

    git clone git@github.com:bengt/deck.js-boilerplate.git

## Dependencies

-   [deck.js](https://github.com/imakewebthings/deck.js)
-   [deck.js-markdown](https://github.com/tmbrggmn/deck.js-markdown)
-   [deck.js-transition-cube](https://github.com/mikeharris100/deck.js-transition-cube)

## Installing dependencies

    cd deck.js-boilerplate
    git submodule update --init

## Running

    cd deck.js-boilerplate
    google-chrome boilerplate.html

